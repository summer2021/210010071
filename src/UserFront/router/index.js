import Vue from 'vue'
import VueRouter from 'vue-router'
import fp1 from '../views/fp1.vue'
import fp2 from '../views/fp2.vue'
import register from '../views/register.vue'
import login from '../views/login.vue'
import test from '../views/test.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'register',
    component: register
  },{
    path: '/login',
    name: 'login',
    component: login
  },{
    path: '/fp1',
    name: 'fp1',
    component: fp1
  },{
    path: '/fp2',
    name: 'fp2',
    component: fp2
  },{
    path: '/test',
    name: 'test',
    component: test
  }
]

const router = new VueRouter({
  routes
})

export default router
