import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

import globalVariable from './api/global_various'

Vue.config.productionTip = false

Vue.prototype.GLOBAL = globalVariable

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
