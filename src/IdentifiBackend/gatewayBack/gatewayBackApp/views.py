from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from gatewayBackApp.models import gateway,sen
import json
import simplejson

import requests

import random
import time
import datetime
from hashlib import md5

from Crypto import Random
from Crypto.PublicKey import RSA
import base64
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher
from pyDes import *

import binascii
import socket
import hmac

from Crypto.Util.number import long_to_bytes,bytes_to_long
from pysmx.SM2 import *


from gmssl.sm4 import CryptSM4, SM4_ENCRYPT, SM4_DECRYPT
from heapq import heappush, heappop
from collections import OrderedDict

# Create your views here.

IDucp = '10001111001100110000111010101001'
len_para = 64
#TCj_haha = ''
#IDj_haha = ''
#UIDj_haha=''

#SM3开始

def rotation_left(x, num):
    # 循环左移
    num %= 32
    left = (x << num) % (2 ** 32)
    right = (x >> (32 - num)) % (2 ** 32)
    result = left ^ right
    return result


def Int2Bin(x, k):
    x = str(bin(x)[2:])
    result = "0" * (k - len(x)) + x
    return result


class SM3:

    def __init__(self):
        # 常量初始化
        self.IV = [0x7380166F, 0x4914B2B9, 0x172442D7, 0xDA8A0600, 0xA96F30BC, 0x163138AA, 0xE38DEE4D, 0xB0FB0E4E]
        self.T = [0x79cc4519, 0x7a879d8a]
        self.maxu32 = 2 ** 32
        self.w1 = [0] * 68
        self.w2 = [0] * 64

    def ff(self, x, y, z, j):
        # 布尔函数FF
        result = 0
        if j < 16:
            result = x ^ y ^ z
        elif j >= 16:
            result = (x & y) | (x & z) | (y & z)
        return result

    def gg(self, x, y, z, j):
        # 布尔函数GG
        result = 0
        if j < 16:
            result = x ^ y ^ z
        elif j >= 16:
            result = (x & y) | (~x & z)
        return result

    def p(self, x, mode):
        result = 0
        # 置换函数P
        # 输入参数X的长度为32bit(=1个字)
        # 输入参数mode共两种取值：0和1
        if mode == 0:
            result = x ^ rotation_left(x, 9) ^ rotation_left(x, 17)
        elif mode == 1:
            result = x ^ rotation_left(x, 15) ^ rotation_left(x, 23)
        return result

    def sm3_fill(self, msg):
        # 填充消息，使其长度为512bit的整数倍
        # 输入参数msg为bytearray类型
        # 中间参数msg_new_bin为二进制string类型
        # 输出参数msg_new_bytes为bytearray类型
        length = len(msg)  # msg的长度（单位：byte）
        l = length * 8  # msg的长度（单位：bit）

        num = length // 64
        remain_byte = length % 64
        msg_remain_bin = ""
        msg_new_bytes = bytearray((num + 1) * 64)  ##填充后的消息长度，单位：byte

        # 将原数据存储至msg_new_bytes中
        for i in range(length):
            msg_new_bytes[i] = msg[i]

        # remain部分以二进制字符串形式存储
        remain_bit = remain_byte * 8  # 单位：bit
        for i in range(remain_byte):
            msg_remain_bin += "{:08b}".format(msg[num * 64 + i])

        k = (448 - l - 1) % 512
        while k < 0:
            # k为满足 l + k + 1 = 448 % 512 的最小非负整数
            k += 512

        msg_remain_bin += "1" + "0" * k + Int2Bin(l, 64)

        for i in range(0, 64 - remain_byte):
            str = msg_remain_bin[i * 8 + remain_bit: (i + 1) * 8 + remain_bit]
            temp = length + i
            msg_new_bytes[temp] = int(str, 2)  # 将2进制字符串按byte为组转换为整数
        return msg_new_bytes

    def sm3_msg_extend(self, msg):
        # 扩展函数: 将512bit的数据msg扩展为132个字（w1共68个字，w2共64个字）
        # 输入参数msg为bytearray类型,长度为512bit=64byte
        for i in range(0, 16):
            self.w1[i] = int.from_bytes(msg[i * 4:(i + 1) * 4], byteorder="big")

        for i in range(16, 68):
            self.w1[i] = self.p(self.w1[i - 16] ^ self.w1[i - 9] ^ rotation_left(self.w1[i - 3], 15),
                                1) ^ rotation_left(self.w1[i - 13], 7) ^ self.w1[i - 6]

        for i in range(64):
            self.w2[i] = self.w1[i] ^ self.w1[i + 4]

        # 测试扩展数据w1和w2
        # print("w1:")
        # for i in range(0, len(self.w1), 8):
        #     print(hex(self.w1[i]))
        # print("w2:")
        # for i in range(0, len(self.w2), 8):
        #     print(hex(self.w2[i]))

    def sm3_compress(self, msg):
        # 压缩函数
        # 输入参数v为初始化参数，类型为bytes/bytearray，大小为256bit
        # 输入参数msg为512bit的待压缩数据

        self.sm3_msg_extend(msg)
        ss1 = 0

        A = self.IV[0]
        B = self.IV[1]
        C = self.IV[2]
        D = self.IV[3]
        E = self.IV[4]
        F = self.IV[5]
        G = self.IV[6]
        H = self.IV[7]

        for j in range(64):
            if j < 16:
                ss1 = rotation_left((rotation_left(A, 12) + E + rotation_left(self.T[0], j)) % self.maxu32, 7)
            elif j >= 16:
                ss1 = rotation_left((rotation_left(A, 12) + E + rotation_left(self.T[1], j)) % self.maxu32, 7)
            ss2 = ss1 ^ rotation_left(A, 12)
            tt1 = (self.ff(A, B, C, j) + D + ss2 + self.w2[j]) % self.maxu32
            tt2 = (self.gg(E, F, G, j) + H + ss1 + self.w1[j]) % self.maxu32
            D = C
            C = rotation_left(B, 9)
            B = A
            A = tt1
            H = G
            G = rotation_left(F, 19)
            F = E
            E = self.p(tt2, 0)

            # 测试IV的压缩中间值
            # print("j= %d：" % j, hex(A)[2:], hex(B)[2:], hex(C)[2:], hex(D)[2:], hex(E)[2:], hex(F)[2:], hex(G)[2:], hex(H)[2:])

        self.IV[0] ^= A
        self.IV[1] ^= B
        self.IV[2] ^= C
        self.IV[3] ^= D
        self.IV[4] ^= E
        self.IV[5] ^= F
        self.IV[6] ^= G
        self.IV[7] ^= H

    def sm3_update(self, msg):
        # 迭代函数
        # 输入参数msg为bytearray类型
        # msg_new为bytearray类型
        msg_new = self.sm3_fill(msg)  # msg_new经过填充后一定是512的整数倍
        n = len(msg_new) // 64  # n是整数，n>=1

        for i in range(0, n):
            self.sm3_compress(msg_new[i * 64:(i + 1) * 64])

    def sm3_final(self):
        digest_str = ""
        for i in range(len(self.IV)):
            digest_str += hex(self.IV[i])[2:]

        return digest_str.upper()

    def hashFile(self, filename):
        with open(filename, 'rb') as fp:
            contents = fp.read()
            self.sm3_update(bytearray(contents))
        return self.sm3_final()

#SM3结束


#基本操作


def str_xor(s1, s2):
  s = ""#结果字符串
  l1 = len(s1)
  l2 = len(s2)
  print(l1, l2)
  for i in range(0, min(l1, l2)):#首先将两个字符串中本来存在的数据进行异或
    # print(s1[i], s2[i])
    a = ord(s1[i])#字符串转十进制
    b = ord(s2[i])
    s = s + chr(a^b)#将异或后结果转字符存到结果字符串中
  # print(s)
  for i in range(min(l1,l2),max(l1,l2)):#将某一字符串多出来的部分进行异或操作
    if(l1<l2):
      s = s + chr(ord(s2[i])^255)
    else :
      s = s + chr(ord(s1[i])^255)#和11111111进行异或
  return s



#通信建立阶段
def build():
    pk, sk = generate_keypair()
    with open('sm2_gate_private_key.pem', 'wb')as f:
        f.write(sk)
    with open('sm2_gate_public_key.pem', 'wb')as f:
        f.write(pk)

def get_key(key_file):
    with open(key_file ,'rb') as f:
        data = f.read()
    return data


def build_ucp_one(request):
    TS1_str = request.POST.get('TS1')
    global r1
    r1 = request.POST.get('r1')
    TS1 = float(TS1_str)
    TS_now = time.time()
    if (TS_now-TS1)<15:
        global r2
        r2 = str(bin(random.randint(0, 99999999999999999999))[2:])
        TS2 = time.time()
        build() #sm2秘钥生成
        Kp = get_key('sm2_gate_public_key.pem')#获取sm2公钥
        post_data = {'r2': r2,'Kp':str(Kp),'TS2':TS2}
        response = requests.post('https://42e58p1445.wicp.vip/userBack/build_user_two', data=post_data)
        return HttpResponse(response)
    else:
        return HttpResponse('fail')

def build_ucp_two(request):
    TS3_str = request.POST.get('TS3')
    ER = eval(request.POST.get('ER'))
    TS3 = float(TS3_str)
    TS_now = time.time()
    if (TS_now-TS3)<15:
        private_key = get_key('sm2_gate_private_key.pem')
        back_text = Decrypt(ER, get_key('sm2_gate_private_key.pem'), len_para)
        res = eval(str(back_text)[2:-1])
        r3 = res["r3"]
        TS3_decode = res["TS3"]
        if (TS3 == TS3_decode):
            global SK
            linshi = str(r1)+str(r2)+str(r3)
            print(linshi)
            step = bytes(linshi.encode('utf-8'))
            msg1 = bytearray(step)
            sm3step2 = SM3()
            sm3step2.sm3_update(msg1)
            sm3res = sm3step2.sm3_final()
            SK = sm3res
            print(SK)
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')

    

#用户注册阶段
#SM4 开始

class SM4:
    """
    国密sm4加解密
    """

    def __init__(self):
        self.crypt_sm4 = CryptSM4()

    def str_to_hexStr(self, hex_str):
        """
        字符串转hex
        :param hex_str: 字符串
        :return: hex
        """
        hex_data = hex_str.encode('utf-8')
        str_bin = binascii.unhexlify(hex_data)
        return str_bin.decode('utf-8')

    def encrypt(self, encrypt_key, value):
        """
        国密sm4加密
        :param encrypt_key: sm4加密key
        :param value: 待加密的字符串
        :return: sm4加密后的hex值
        """
        crypt_sm4 = self.crypt_sm4
        crypt_sm4.set_key(encrypt_key.encode(), SM4_ENCRYPT)
        date_str = str(value)
        encrypt_value = crypt_sm4.crypt_ecb(date_str.encode())  # bytes类型
        #return encrypt_value.hex()
        return base64.b64encode(encrypt_value)

    def decrypt(self, decrypt_key, encrypt_value):
        """
        国密sm4解密
        :param decrypt_key:sm4加密key
        :param encrypt_value: 待解密的hex值
        :return: 原字符串
        """
        crypt_sm4 = self.crypt_sm4
        crypt_sm4.set_key(decrypt_key.encode(), SM4_DECRYPT)
        #decrypt_value = crypt_sm4.crypt_ecb(bytes.fromhex(encrypt_value))  # bytes类型
        decrypt_value = crypt_sm4.crypt_ecb(base64.b64decode(encrypt_value))
        return self.str_to_hexStr(decrypt_value.hex())

#SM4 结束

def user_register(request):
    TS1_str = request.POST.get('TS1')
    EIDi = eval(request.POST.get('EIDi'))
    TS_now = time.time()
    TS1 = float(TS1_str)
    if (TS_now-TS1)<15:
        back_text = SM4().decrypt(SK, EIDi)
        res = eval(back_text)
        UIDi = res["UIDi"]
        print('------------------')
        print('UIDi:'+UIDi)
        print('------------------')
        TS1_decode = res["TS1"]
        if (TS1 == TS1_decode):
            s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            s.connect(('47.108.195.179',34126))
            s.send(bytes(UIDi.encode('utf-8')))
            print(UIDi.encode('utf-8'))
            PIDi=s.recv(1024)
            testhaha = ''
            for i in PIDi:
                testhaha += '%02x' % i
            s.close()
            print(testhaha)
            PIDi = testhaha
            isRegister = gateway.objects.filter(PIDi = PIDi).exists()
            if isRegister:#在列表中说明用户已经注册过了
                return HttpResponse('has_reg')
            else:
                TS2 = time.time()
                linshi = str(IDucp)+str(PIDi)
                step = bytes(linshi.encode('utf-8'))
                msg1 = bytearray(step)
                sm3step2 = SM3()
                sm3step2.sm3_update(msg1)
                sm3res = sm3step2.sm3_final()
                V = sm3res
                first_test = {"TS2":TS2}
                se_test = json.dumps(first_test)
                ACKucp = SM4().encrypt(SK, se_test)
                myGWN = gateway(PIDi = PIDi,V = V)
                myGWN.save()
                post_data = {'ACKucp': str(ACKucp),'TS2':TS2}
                response = requests.post('https://42e58p1445.wicp.vip/userBack/register_two', data=post_data)
                return HttpResponse(response)
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')




#功能平台注册阶段
def sensor_register(request):
    TS1 = float(request.POST.get('TS1'))
    IDj = request.POST.get('IDj')
    TS_now = time.time()
    if (TS_now-TS1)<15:
        Rj = str(bin(random.randint(0, 99999999999999999999))[2:])
        linshi = str(IDj)+str(Rj)
        step = bytes(linshi.encode('utf-8'))
        msg1 = bytearray(step)
        sm3step2 = SM3()
        sm3step2.sm3_update(msg1)
        sm3res = sm3step2.sm3_final()
        TCj = sm3res
        linshi = str(IDj)+str(IDucp)
        step = bytes(linshi.encode('utf-8'))
        msg1 = bytearray(step)
        sm3step2 = SM3()
        sm3step2.sm3_update(msg1)
        sm3res = sm3step2.sm3_final()
        UIDj = sm3res
        PTCj = str_xor(TCj,UIDj)
        print('IDj:'+IDj)
        IDj_haha = IDj
        print('PTCJ:'+PTCj)
        print('Rj:'+Rj)
        print('TCj:'+TCj)
        TS2 = time.time()
        print(bytes(IDj.encode('utf-8')))
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        s.connect(('47.108.195.179',34126))
        s.send(bytes(IDj.encode('utf-8')))
        print(IDj.encode('utf-8'))
        IDj=s.recv(1024)
        testhaha = ''
        for i in IDj:
            testhaha += '%02x' % i
        s.close()
        print(testhaha)
        PIDj = testhaha
        s.close()
        print('-------------------start------------------')
        print('Rj:'+Rj)
        print('IDucp:'+IDucp)
        PRj = str_xor(Rj,IDucp)
        print('PRj:'+PRj)
        print('-------------------end--------------------')
        mySEN = sen(PIDj = PIDj,PRj = PRj)
        mySEN.save()
        post_data = {'PTCj': PTCj,'TS2':TS2}
        if IDj_haha == '201f114d70e54c5b6764aa0d6bb62a05':
            response = requests.post('https://42e58p1445.wicp.vip/sensorBack/register_two', data=post_data)
            return HttpResponse(response)
        elif IDj_haha == '36b876925d28488346d08b38f7513d89':
            response = requests.post('https://42e58p1445.wicp.vip/sensorBack2/register_two', data=post_data) 
            return HttpResponse(response)
        else:
            return HttpResponse('fail')


#登录阶段
            
def user_login(request):
    EIDi = eval(request.POST.get('EIDi'))
    TS1 = float(request.POST.get('TS1'))
    TS_now = time.time()
    if (TS_now-TS1)<15:
        back_text = SM4().decrypt(SK, EIDi)
        res = eval(back_text)
        UIDi = res["UIDi"]
        IDj = res["IDj"]
        global IDj_haha
        IDj_haha = IDj
        Ni = res["Ni"]
        global Ni_haha
        Ni_haha = Ni
        TS1_decode = res["TS1"]
        if (TS1 == TS1_decode):
            s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            s.connect(('47.108.195.179',34126))
            s.send(bytes(UIDi.encode('utf-8')))
            print(UIDi.encode('utf-8'))
            PIDi=s.recv(1024)
            testhaha = ''
            for i in PIDi:
                testhaha += '%02x' % i
            s.close()
            print(testhaha)
            PIDi = testhaha
            print(PIDi)
            s.close()
            isRegister = gateway.objects.filter(PIDi = PIDi).exists()
            if isRegister:
                V = gateway.objects.get(PIDi = PIDi).V
                linshi = str(IDucp)+str(PIDi)
                step = bytes(linshi.encode('utf-8'))
                msg1 = bytearray(step)
                sm3step2 = SM3()
                sm3step2.sm3_update(msg1)
                sm3res = sm3step2.sm3_final()
                V_s = sm3res
                if (V == V_s):
                    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                    s.connect(('47.108.195.179',34126))
                    s.send(bytes(IDj.encode('utf-8')))
                    print('aaaaaaaaaaaa')
                    print(IDj.encode('utf-8'))
                    PIDj=s.recv(1024)
                    testhaha = ''
                    for i in PIDj:
                        testhaha += '%02x' % i
                    s.close()
                    PIDj = testhaha
                    s.close()
                    PRj = sen.objects.get(PIDj = PIDj).PRj
                    Rj = str_xor(PRj,IDucp)
                    linshi = str(IDj)+str(Rj)
                    step = bytes(linshi.encode('utf-8'))
                    msg1 = bytearray(step)
                    sm3step2 = SM3()
                    sm3step2.sm3_update(msg1)
                    sm3res = sm3step2.sm3_final()
                    TCj = sm3res
                    global TCj_haha
                    TCj_haha = TCj
                    print('Rj:'+Rj)
                    linshi = str(IDj)+str(IDucp)
                    step = bytes(linshi.encode('utf-8'))
                    msg1 = bytearray(step)
                    sm3step2 = SM3()
                    sm3step2.sm3_update(msg1)
                    sm3res = sm3step2.sm3_final()
                    UIDj = sm3res
                    global UIDj_haha
                    UIDj_haha = UIDj
                    TS2 = time.time()
                    linshi = str(TCj)+str(IDj)+str(TS2)
                    step = bytes(linshi.encode('utf-8'))
                    msg1 = bytearray(step)
                    sm3step2 = SM3()
                    sm3step2.sm3_update(msg1)
                    sm3res = sm3step2.sm3_final()
                    TRN1 = str_xor(Ni,sm3res)
                    first_test = {'TRN1':TRN1,'TCj':TCj,'UIDj':UIDj,'TS2':TS2}
                    se_test = json.dumps(first_test)
                    print(se_test)
                    mac = hmac.new(bytes(Ni.encode("utf8")),bytes(se_test.encode("utf8")),md5)
                    print('Ni:'+Ni)
                    q1 = mac.hexdigest()
                    post_data = {'UIDj': UIDj,'TRN1':TRN1,'q1':q1,'TS2':TS2}
                    print('aaaaaaaaab'+q1)
                    if IDj == '201f114d70e54c5b6764aa0d6bb62a05':
                        response = requests.post('https://42e58p1445.wicp.vip/sensorBack/login', data=post_data)
                    elif IDj == '36b876925d28488346d08b38f7513d89':
                        response = requests.post('https://42e58p1445.wicp.vip/sensorBack2/login', data=post_data)
                    else:
                        response = 'fp_error'
                    return HttpResponse(response)
                else:
                    return HttpResponse('password_error')
            else:
                return HttpResponse('not_register')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
        
    

def login_two(request):
    TRN2 = request.POST.get('TRN2')
    q2 = request.POST.get('q2')
    TS3 = float(request.POST.get('TS3'))
    TS_now = time.time()
    if (TS_now-TS3)<15:
        linshi = str(Ni_haha)+str(TCj_haha)+str(IDj_haha)+str(TS3)
        step = bytes(linshi.encode('utf-8'))
        msg1 = bytearray(step)
        sm3step2 = SM3()
        sm3step2.sm3_update(msg1)
        sm3res = sm3step2.sm3_final()
        Nj = str_xor(TRN2,sm3res)
        first_test = {'TRN2':TRN2,'Ni':Ni_haha,'TCj':TCj_haha,'UIDj':UIDj_haha,'TS3':TS3}
        se_test = json.dumps(first_test)
        mac = hmac.new(bytes(Nj.encode("utf8")),bytes(se_test.encode("utf8")),md5)
        q2_s = mac.hexdigest()
        print('============================')
        print(first_test)
        print('q2_s:'+q2_s)
        print('q2:'+q2)
        print('Ni:'+Ni_haha)
        print('Nj:'+Nj)
        print('============================')
        if q2 == q2_s:
            print('=====================yes==================')
            TS4 = time.time()
            first_test = {"Nj":Nj,"TS4":TS4}
            se_test = json.dumps(first_test)
            ENj = SM4().encrypt(SK, se_test)
            post_data = {'ENj': str(ENj),'TS4':TS4}
            response = requests.post('https://42e58p1445.wicp.vip/userBack/login_two', data=post_data)
            return HttpResponse(response)
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
"""
def user_register(request):
    TS1 = request.POST.get('TS1')
    ri = request.POST.get('ri')
    IDsc = request.POST.get('IDsc')
    RPWi = request.POST.get('RPWi')
    TS_now = time.time()
    if (TS_now-TS1)<9:
        IDgwn = '0111010101010111010111011101000'
        Ku = '10001111001100110000111010101001'
        DKi = bin(random.randint(0, 9999999999999999999999999999))[2:]
        TCi = md5(str_or(DKi,ri)).encode('utf-8')).hexdigest()
        Bi = md5(str_or(IDsc,RPWi)).encode('utf-8')).hexdigest()
        PTCi = str_xor(TCi,md5(str_or(ri,IDsc)).encode('utf-8')).hexdigest())
        PDKi = str_xor(DKi,ri)
        BNi = str_xor(ri,md5(str_or(str_or（PTCi,IDgwn),Ku)).encode('utf-8')).hexdigest())
        TS2 = time.time()
        post_data = {'Bi': Bi,'PTCi':PTCi,'TS2':TS2}
        response = requests.post('http://localhost:8000/register_receive', data=post_data)
        myGWN = gateway(PTCi = PTCi,PDKi = PDKi,BNi = BNi)
        myGWN.save()

def sen_register(request):
    TS3 = request.POST.get('TS3')
    IDj = request.POST.get('IDj')
    TS_now = time.time()
    if (TS_now-TS3)<9:
        IDgwn = '0111010101010111010111011101000'
        rj = bin(random.randint(0, 9999999999999999999999999999))[2:]
        DKj = bin(random.randint(0, 9999999999999999999999999999))[2:]
        Ksn = '10001111001100110000111010101001'
        TCj = md5(str_or(DKj,rj)).encode('utf-8')).hexdigest()
        PIDj = md5(str_or(DKj,IDj)).encode('utf-8')).hexdigest()
        PTCj = str_xor(TCj,PIDj)
        KIDj = str_xor(IDj,IDgwn)
        PDKj = str_xor(DKj,rj)
        CNj = str_xor(rj,md5(str_or(IDj,str_or(IDgwn,Ksn))).encode('utf-8')).hexdigest())
        TS4 = time.time()
        post_data = {'PTCj': PTCj,'TS4':TS4}
        response = requests.post('http://localhost:8003/register_receive', data=post_data)
        myGWN = gateway(KIDj = KIDj,CNj = CNj,PDKj = PDKj)
        myGWN.save()

def user_login(request):
    TS1 = request.POST.get('TS1')
    PTCi = request.POST.get('PTCi')
    TS_now = time.time()
    if (TS_now-TS1)<9:
        IDgwn = '0111010101010111010111011101000'
        myGate = gateway.objects.get(PTCi = PTCi)
        PDKi = myGate.PDKi
        BNi = myGate.BNi
        Ku = '10001111001100110000111010101001'
        ri = str_xor(BNi,md5(str_or(PTCi,str_or(IDgwn,Ku))).encode('utf-8')).hexdigest())
        DKi = str_xor(PDKi,ri)
        TCi = md5(str_or(DKi,ri)).encode('utf-8')
        Ni = str_xor(PKSi,md5(str_or(TCi,str_or(ri,TS1))).encode('utf-8'))
        IDj = str_xor(PIDj,md5(str_or(TCi,str_or(TS1,Ni))).encode('utf-8'))
        KIDj = str_xor(IDj,IDgwn)
        q1_ = md5(str_or(TCi,str_or(IDj,str_or(Ni,ri)))).encode('utf-8')
        if q1_ == q1:
            myGate2 = gateway.objects.get(KIDj = KIDj)
            PDKj = myGate2.PDKj
            CNj = myGate2.CNj
            Ksn = '10001111001100110000111010101001'
            rj = str_xor(CNj,md5(str_or(IDj,str_or(IDgwn,Ksn))).encode('utf-8')).hexdigest())
            DKj = str_xor(PDKj,rj)
            TCj = md5(str_or(DKj,ri)).encode('utf-8')).hexdigest()
            PIDj = md5(str_or(DKj,IDj)).encode('utf-8')).hexdigest()
            q2 = md5(str_or(str_xor(TCj,Ni),IDj)).encode('utf-8')).hexdigest()
            TS2 = time.time()
            PKSn = str_xor(Ni,md5(str_or(TCj,str_or(IDj,TS2))).encode('utf-8')).hexdigest())
            post_data = {'q2': q2,'PKSn':PKSn,'PIDj':PIDj,'TS2',TS2,'IDj':IDj}
            response = requests.post('http://localhost:8003/login', data=post_data)

def sensor_login_receive(request):
    TS3 = request.POST.get('TS3')
    q3 = request.POST.get('q3')
    PKSj = request.POST.get('PKSj')
    TS_now = time.time()
    if (TS_now-TS3)<9:
        Kj = str_xor(PKSj,md5(str_or(str_or(TCi,Ni),str_or(IDj,TS3))).encode('utf-8')).hexdigest())
        q3_ = md5(str_or(str_or(Ni,str_or(Kj,str_xor(TCj,IDj))))).encode('utf-8')).hexdigest()
        if q3_ == q3:
            PKSk = str_xor(Kj,md5(str_or(TCi,str_xor(Ni,IDj)).encode('utf-8')).hexdigest())
            TS4 = time.time()
            q4 = md5(str_or(TCi,str_or(str_xor(ri,Ni),TS4))).encode('utf-8')).hexdigest()
            post_data = {'q4': q4,'PKSk':PKSk,'TS4',TS4}
            response = requests.post('http://localhost:8000/login_receive', data=post_data)

"""
