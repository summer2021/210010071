from django.apps import AppConfig


class GatewaybackappConfig(AppConfig):
    name = 'gatewayBackApp'
