from django.db import models

# Create your models here.

class sensor(models.Model):
    sid = models.AutoField('结点编号',primary_key=True)
    PTCj = models.CharField('PTCj',max_length=1000,null=True)
    IDj = models.CharField('PTCj',max_length=20,null=True)

class Meta:
     verbose_name = 'sensor'
     verbose_name_plural = 'sensor'
     ordering = ['sid']
