from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from userBackApp.models import user
import json
import simplejson
import requests

import random
import time
import datetime
from hashlib import md5

from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher
import base64
import rsa
from pyDes import *

import binascii
import hmac

from Crypto.Util.number import long_to_bytes,bytes_to_long

from pysmx.SM2 import *

from gmssl.sm4 import CryptSM4, SM4_ENCRYPT, SM4_DECRYPT
from heapq import heappush, heappop
from collections import OrderedDict

from django.views.decorators.csrf import csrf_exempt
from PIL import Image


import os
import cv2
import numpy as np
from userBackApp.utils import index2emotion, expression_analysis, cv2_img_add_text

# Create your views here.

#white_list = ['127.0.0.1']

len_para = 64

#SM3开始

def rotation_left(x, num):
    # 循环左移
    num %= 32
    left = (x << num) % (2 ** 32)
    right = (x >> (32 - num)) % (2 ** 32)
    result = left ^ right
    return result


def Int2Bin(x, k):
    x = str(bin(x)[2:])
    result = "0" * (k - len(x)) + x
    return result


class SM3:

    def __init__(self):
        # 常量初始化
        self.IV = [0x7380166F, 0x4914B2B9, 0x172442D7, 0xDA8A0600, 0xA96F30BC, 0x163138AA, 0xE38DEE4D, 0xB0FB0E4E]
        self.T = [0x79cc4519, 0x7a879d8a]
        self.maxu32 = 2 ** 32
        self.w1 = [0] * 68
        self.w2 = [0] * 64

    def ff(self, x, y, z, j):
        # 布尔函数FF
        result = 0
        if j < 16:
            result = x ^ y ^ z
        elif j >= 16:
            result = (x & y) | (x & z) | (y & z)
        return result

    def gg(self, x, y, z, j):
        # 布尔函数GG
        result = 0
        if j < 16:
            result = x ^ y ^ z
        elif j >= 16:
            result = (x & y) | (~x & z)
        return result

    def p(self, x, mode):
        result = 0
        # 置换函数P
        # 输入参数X的长度为32bit(=1个字)
        # 输入参数mode共两种取值：0和1
        if mode == 0:
            result = x ^ rotation_left(x, 9) ^ rotation_left(x, 17)
        elif mode == 1:
            result = x ^ rotation_left(x, 15) ^ rotation_left(x, 23)
        return result

    def sm3_fill(self, msg):
        # 填充消息，使其长度为512bit的整数倍
        # 输入参数msg为bytearray类型
        # 中间参数msg_new_bin为二进制string类型
        # 输出参数msg_new_bytes为bytearray类型
        length = len(msg)  # msg的长度（单位：byte）
        l = length * 8  # msg的长度（单位：bit）

        num = length // 64
        remain_byte = length % 64
        msg_remain_bin = ""
        msg_new_bytes = bytearray((num + 1) * 64)  ##填充后的消息长度，单位：byte

        # 将原数据存储至msg_new_bytes中
        for i in range(length):
            msg_new_bytes[i] = msg[i]

        # remain部分以二进制字符串形式存储
        remain_bit = remain_byte * 8  # 单位：bit
        for i in range(remain_byte):
            msg_remain_bin += "{:08b}".format(msg[num * 64 + i])

        k = (448 - l - 1) % 512
        while k < 0:
            # k为满足 l + k + 1 = 448 % 512 的最小非负整数
            k += 512

        msg_remain_bin += "1" + "0" * k + Int2Bin(l, 64)

        for i in range(0, 64 - remain_byte):
            str = msg_remain_bin[i * 8 + remain_bit: (i + 1) * 8 + remain_bit]
            temp = length + i
            msg_new_bytes[temp] = int(str, 2)  # 将2进制字符串按byte为组转换为整数
        return msg_new_bytes

    def sm3_msg_extend(self, msg):
        # 扩展函数: 将512bit的数据msg扩展为132个字（w1共68个字，w2共64个字）
        # 输入参数msg为bytearray类型,长度为512bit=64byte
        for i in range(0, 16):
            self.w1[i] = int.from_bytes(msg[i * 4:(i + 1) * 4], byteorder="big")

        for i in range(16, 68):
            self.w1[i] = self.p(self.w1[i - 16] ^ self.w1[i - 9] ^ rotation_left(self.w1[i - 3], 15),
                                1) ^ rotation_left(self.w1[i - 13], 7) ^ self.w1[i - 6]

        for i in range(64):
            self.w2[i] = self.w1[i] ^ self.w1[i + 4]

        # 测试扩展数据w1和w2
        # print("w1:")
        # for i in range(0, len(self.w1), 8):
        #     print(hex(self.w1[i]))
        # print("w2:")
        # for i in range(0, len(self.w2), 8):
        #     print(hex(self.w2[i]))

    def sm3_compress(self, msg):
        # 压缩函数
        # 输入参数v为初始化参数，类型为bytes/bytearray，大小为256bit
        # 输入参数msg为512bit的待压缩数据

        self.sm3_msg_extend(msg)
        ss1 = 0

        A = self.IV[0]
        B = self.IV[1]
        C = self.IV[2]
        D = self.IV[3]
        E = self.IV[4]
        F = self.IV[5]
        G = self.IV[6]
        H = self.IV[7]

        for j in range(64):
            if j < 16:
                ss1 = rotation_left((rotation_left(A, 12) + E + rotation_left(self.T[0], j)) % self.maxu32, 7)
            elif j >= 16:
                ss1 = rotation_left((rotation_left(A, 12) + E + rotation_left(self.T[1], j)) % self.maxu32, 7)
            ss2 = ss1 ^ rotation_left(A, 12)
            tt1 = (self.ff(A, B, C, j) + D + ss2 + self.w2[j]) % self.maxu32
            tt2 = (self.gg(E, F, G, j) + H + ss1 + self.w1[j]) % self.maxu32
            D = C
            C = rotation_left(B, 9)
            B = A
            A = tt1
            H = G
            G = rotation_left(F, 19)
            F = E
            E = self.p(tt2, 0)

            # 测试IV的压缩中间值
            # print("j= %d：" % j, hex(A)[2:], hex(B)[2:], hex(C)[2:], hex(D)[2:], hex(E)[2:], hex(F)[2:], hex(G)[2:], hex(H)[2:])

        self.IV[0] ^= A
        self.IV[1] ^= B
        self.IV[2] ^= C
        self.IV[3] ^= D
        self.IV[4] ^= E
        self.IV[5] ^= F
        self.IV[6] ^= G
        self.IV[7] ^= H

    def sm3_update(self, msg):
        # 迭代函数
        # 输入参数msg为bytearray类型
        # msg_new为bytearray类型
        msg_new = self.sm3_fill(msg)  # msg_new经过填充后一定是512的整数倍
        n = len(msg_new) // 64  # n是整数，n>=1

        for i in range(0, n):
            self.sm3_compress(msg_new[i * 64:(i + 1) * 64])

    def sm3_final(self):
        digest_str = ""
        for i in range(len(self.IV)):
            digest_str += hex(self.IV[i])[2:]

        return digest_str.upper()

    def hashFile(self, filename):
        with open(filename, 'rb') as fp:
            contents = fp.read()
            self.sm3_update(bytearray(contents))
        return self.sm3_final()

#SM3结束





#通信建立阶段


def build_user_one():
    global r1
    r1 = str(bin(random.randint(0, 99999999999999999999))[2:])
    TS1 = time.time()
    post_data = {'r1': r1,'TS1':TS1}
    response = requests.post('https://42e58p1445.wicp.vip/gatewayBack/build_ucp_one', data=post_data)
    return response


def get_key(key_file):
    with open(key_file ,'rb') as f:
        data = f.read()
    return data

def build_user_two(request):
    TS2_str = request.POST.get('TS2')
    r2 = request.POST.get('r2')
    Kp = request.POST.get('Kp')
    TS2 = float(TS2_str)
    TS_now = time.time()
    if (TS_now-TS2)<15:
        r3 = str(bin(random.randint(0, 99999999999999999999))[2:])
        TS3 = time.time()
        with open('sm2_gate_public_key.pem', 'wb')as f:
            f.write(eval(Kp))
        first_test = {'r3':r3,'TS3':TS3}
        se_test = json.dumps(first_test)
        ER = Encrypt(bytes(se_test.encode('utf-8')), get_key('sm2_gate_public_key.pem'), len_para, 0)
        global SK
        linshi = str(r1)+str(r2)+str(r3)
        step = bytes(linshi.encode('utf-8'))
        msg1 = bytearray(step)
        sm3step2 = SM3()
        sm3step2.sm3_update(msg1)
        sm3res = sm3step2.sm3_final()
        SK = sm3res
        print(SK)
        post_data = {'ER': str(ER),'TS3':TS3}
        response = requests.post('https://42e58p1445.wicp.vip/gatewayBack/build_ucp_two', data=post_data)
        return HttpResponse(response)
    else:
        return HttpResponse('fail')

#注册阶段

#SM4 开始

class SM4:
    """
    国密sm4加解密
    """

    def __init__(self):
        self.crypt_sm4 = CryptSM4()

    def str_to_hexStr(self, hex_str):
        """
        字符串转hex
        :param hex_str: 字符串
        :return: hex
        """
        hex_data = hex_str.encode('utf-8')
        str_bin = binascii.unhexlify(hex_data)
        return str_bin.decode('utf-8')

    def encrypt(self, encrypt_key, value):
        """
        国密sm4加密
        :param encrypt_key: sm4加密key
        :param value: 待加密的字符串
        :return: sm4加密后的hex值
        """
        crypt_sm4 = self.crypt_sm4
        crypt_sm4.set_key(encrypt_key.encode(), SM4_ENCRYPT)
        date_str = str(value)
        encrypt_value = crypt_sm4.crypt_ecb(date_str.encode())  # bytes类型
        #return encrypt_value.hex()
        return base64.b64encode(encrypt_value)

    def decrypt(self, decrypt_key, encrypt_value):
        """
        国密sm4解密
        :param decrypt_key:sm4加密key
        :param encrypt_value: 待解密的hex值
        :return: 原字符串
        """
        crypt_sm4 = self.crypt_sm4
        crypt_sm4.set_key(decrypt_key.encode(), SM4_DECRYPT)
        #decrypt_value = crypt_sm4.crypt_ecb(bytes.fromhex(encrypt_value))  # bytes类型
        decrypt_value = crypt_sm4.crypt_ecb(base64.b64decode(encrypt_value))
        return self.str_to_hexStr(decrypt_value.hex())

#SM4 结束
        
def register_one(request):
    build_user_one()
    IDi = request.POST.get('IDi')
    PWi = request.POST.get('PWi')
    linshi = str(IDi)+str(PWi)
    step = bytes(linshi.encode('utf-8'))
    msg1 = bytearray(step)
    sm3step2 = SM3()
    sm3step2.sm3_update(msg1)
    sm3res = sm3step2.sm3_final()
    UIDi = sm3res
    print('------------------')
    print('UIDi:'+UIDi)
    print('------------------')
    TS1 = time.time()
    first_test = {'UIDi':UIDi,'TS1':TS1}
    se_test = json.dumps(first_test)
    #SM4 = SM4()
    EIDi=SM4().encrypt(SK, se_test)
    post_data = {'EIDi': str(EIDi),'TS1':TS1}
    response = requests.post('https://42e58p1445.wicp.vip/gatewayBack/user_register', data=post_data)
    return HttpResponse(response)

def register_two(request):
    ACKucp = eval(request.POST.get('ACKucp'))
    TS2_str = request.POST.get('TS2')
    TS2 = float(TS2_str)
    TS_now = time.time()
    if (TS_now-TS2)<15:
        back_text = SM4().decrypt(SK, ACKucp)
        res = eval(back_text)
        TS2_decode = res["TS2"]
        if (TS2 == TS2_decode):
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
            


#登录阶段
def login_one(request):
    res = build_user_one()
    print(res.content)
    if(str(res.content)[2:-1]) == "fail":
        return HttpResponse("build_fail")
    IDi = request.POST.get('IDi')
    IDj = request.POST.get('IDj')
    PWi = request.POST.get('PWi')
    print(request.POST.get('IDi'))
    print(PWi)
    Ni = bin(random.randint(0, 99999999999999999999))[2:]
    global Ni_haha
    Ni_haha = Ni
    TS1 = time.time()
    linshi = str(IDi)+str(PWi)
    step = bytes(linshi.encode('utf-8'))
    msg1 = bytearray(step)
    sm3step2 = SM3()
    sm3step2.sm3_update(msg1)
    sm3res = sm3step2.sm3_final()
    UIDi = sm3res
    first_test = {'UIDi':UIDi,'IDj':IDj,'Ni':Ni,'TS1':TS1}
    se_test = json.dumps(first_test)
    EIDi = SM4().encrypt(SK, se_test)
    post_data = {'EIDi': str(EIDi),'TS1':TS1}
    response = requests.post('https://42e58p1445.wicp.vip/gatewayBack/user_login', data=post_data)
    return HttpResponse(response)

def login_two(request):
    ENj = eval(request.POST.get('ENj'))
    TS4 = float(request.POST.get('TS4'))
    TS_now = time.time()
    if (TS_now-TS4)<15:
        back_text = SM4().decrypt(SK, ENj)
        res = eval(back_text)
        Nj = res["Nj"]
        TS4_decode = res["TS4"]
        if (TS4 == TS4_decode):
            linshi = str(Ni_haha)+str(Nj)
            step = bytes(linshi.encode('utf-8'))
            msg1 = bytearray(step)
            sm3step2 = SM3()
            sm3step2.sm3_update(msg1)
            sm3res = sm3step2.sm3_final()
            KEYij = sm3res
            print('---------------------')
            print('KEYij:'+KEYij)
            print('---------------------')
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
    


    
"""

def register_post(request):
    IDsc = str2bin(request.POST.get('IDsc'))
    IDi = str2bin(request.POST.get('IDi'))
    PWi = str2bin(request.POST.get('PWi'))
    a = bin(random.randint(0, 9999999999999999999999999999))[2:]
    global TS1
    TS1 = time.time()
    first = str2bin(md5(str_or(str_or(IDsc,IDi),PWi).encode('utf-8')).hexdigest())
    global ANi
    ANi = str_xor(ri,first)
    RPWi = md5(str_or(str_or(IDsc,ri),PWi).encode('utf-8')).hexdigest()
    post_data = {'IDsc': IDsc,'ri':ri,'RPWi':RPWi,'TS1':TS1}
    response = requests.post('http://localhost:8001/user_register', data=post_data)
    
def register_receive(request):
    TS2 = request.POST.get('TS2')
    Bi = request.POST.get('Bi')
    PCTi = request.POST.get('PCTi')
    if (TS2-TS1)<9:
        hex_ = md5(ANi+Bi+PTCi).hexdigest()
        myUser = user(ANi = ANi,Bi = Bi,PTCi = PTCi,h = hex_)
        myUser.save()

def login(request):
    IDsc = str2bin(request.POST.get('IDsc'))
    IDi = str2bin(request.POST.get('IDi'))
    IDj = str2bin(request.POST.get('IDj'))
    PWi = str2bin(request.POST.get('PWi'))
    myUser = user.objects.get(IDsc = IDsc,IDi = IDi)
    ANi = myUser.ANi
    Bi = myUser.Bi
    ri = str_xor(ANi,md5(str_or(str_or(IDsc,IDi),PWi).encode('utf-8')).hexdigest())
    RPWi = md5(str_or(str_or(IDsc,ri),PWi).encode('utf-8')).hexdigest()
    Bi_ = md5(str_or(IDsc,RPWi).encode('utf-8')).hexdigest()
    if Bi_ == Bi:
        Ni = bin(random.randint(0, 9999999999999999999999999999))[2:]
        TS1 = time.time()
        TCi = str_xor(PTCi,md5(str_or(ri,IDsc).encode('utf-8')).hexdigest())
        q1 = md5(str_or(TCi,str_or(IDj,str_or(Ni,ri))).encode('utf-8')).hexdigest()
        PKSi = str_xor(Ni,md5(str_or(TCi,str_or(ri,TS1)).encode('utf-8')).hexdigest())
        PIDj = str_xor(IDj,md5(str_or(TCi,str_or(TS1,Ni)).encode('utf-8')).hexdigest())
        post_data = {'q1': q1,'PKSi':PKSi,'PIDj':PIDj,'PTCi':PTCi,'TS1':TS1}
        response = requests.post('http://localhost:8001/user_login', data=post_data)
        
def login_receive(request):
    TS4 = request.POST.get('TS4')
    q4 = request.POST.get('q4')
    PKSk = request.POST.get('PKSk')
    TS_now = time.time()
    if (TS_now-TS4)<9:
        q4_ = md5(str_or(str_or(TCi,str_xor(ri,Ni)),TS4).encode('utf-8')).hexdigest()
        if q4_ == q4:
            Kj = str_xor(PKSk,md5(str_or(TCi,str_xor(Ni,IDj)).encode('utf-8')).hexdigest())
            KEYij = md5(str_or(Ni,Kj),TS4).encode('utf-8')).hexdigest()
    
    
"""

