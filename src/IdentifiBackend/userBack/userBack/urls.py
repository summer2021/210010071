"""userBack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
#from userBackApp.views import register_post,register_receive,login,login_receive
from userBackApp.views import build_user_one,build_user_two
from userBackApp.views import register_one,register_two
from userBackApp.views import login_one,login_two


urlpatterns = [
    path('admin/', admin.site.urls),
    path('build_user_one',build_user_one),
    path('build_user_two',build_user_two),
    path('register_one',register_one),
    path('register_two',register_two),
    path('login_one',login_one),
    path('login_two',login_two),
]
