#!/bin/bash
#set +x

if [ ! -d "./enclave/lib" ];then
    mkdir ./enclave/lib
fi
cmake -DCMAKE_BUILD_TYPE=debug -DCC_SGX=ON -DSGXSDK=$SGX_SDK  ./ && make