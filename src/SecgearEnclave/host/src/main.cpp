#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <thread>
#include <string>
#include <ctime>
#include "enclave.h"
#include "../Enclave_u.h"

#define MAX_CLIENT 30
#define HASH_LEN 32
#define ID_LEN 32
#define BUF_SIZE 64
#define JOIN_LEN 192
#define LISTEN_PORT 54632

void calculate(cc_enclave_t *context, int clnt_sock, std::string time);
void print(unsigned char *bytes, int len, std::string times, int op);

int main()
{
    int retval = 0;
    cc_enclave_t *context = NULL;
    cc_enclave_result_t res;

    res = cc_enclave_create(PATH, AUTO_ENCLAVE_TYPE, 0, SECGEAR_DEBUG_FLAG, NULL, 0, &context);
    if (res != CC_SUCCESS || retval != (int)CC_SUCCESS)
    {
        printf("Create enclave error\n");
        return res;
    }
    printf("Create enclave successfully!\n");
    // create TCP socket
    int serv_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (serv_socket < 0)
    {
        printf("Create server socket error: %s \n", strerror(errno));
        exit(-1);
    }
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    //serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(LISTEN_PORT);
    if (::bind(serv_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("bind data socket failed: %s\n", strerror(errno));
        exit(-1);
    }
    printf("Bind data socket success!\n");
    if (::listen(serv_socket, MAX_CLIENT) < 0)
    {
        printf("listen data stream failed: %s\n", strerror(errno));
        exit(-1);
    }
    int error_count = 3;
    printf("The Master Key is as follows:\n");
    ecall_print(context);
    int count = 0;
    printf("START LISTENING!\n");
    while (true)
    {
        struct sockaddr_in clnt_addr;
        int clnt_addrl = sizeof(clnt_addr);
        int clnt_sock = accept(serv_socket, (struct sockaddr *)&clnt_addr, (socklen_t *)&clnt_addrl);
        if (clnt_sock < 0)
        {
            printf("accept error:%s\n", strerror(errno));
            error_count--;
            if (error_count == 0)
                exit(0);
            continue;
        }
        printf("Got a client socket!");
        //printf("Got a client socket:_%d_with InetAddress: %s:%d\n", clnt_sock, inet_ntoa(clnt_addr.sin_addr), ntohs(clnt_addr.sin_port));
        time_t tt = time(0);
        char *stime = ctime(&tt);
        std::string time = stime;
        std::string tips = "Got a client socket!***The Calculate Time is ";
        tips += time;
        std::cout << tips;
        std::thread t(calculate, context, clnt_sock, time);
        t.detach();
    }
    close(serv_socket);
    pthread_exit(NULL);
    if (context != NULL)
        res = cc_enclave_destroy(context);
    return res;
}

void calculate(cc_enclave_t *context, int clnt_sock, std::string t)
{
    std::string time = t;
    char buf[BUF_SIZE] = {0};                                       //buffer
    unsigned char *resl = (unsigned char *)malloc(JOIN_LEN);        //accept connection result of UIDi and UIDj
    unsigned char *hash_result = (unsigned char *)malloc(HASH_LEN); //accept hash result
    try
    {
        read(clnt_sock, buf, ID_LEN);
        std::string s;
        s += "Calc[";
        s += time;
        s += "]: Got Message From Client OR Function Platform\n";
        std::cout << s;
        unsigned char *UIDi = (unsigned char *)buf;
        print(UIDi, ID_LEN, time, 0);
        conn_str(context, UIDi, ID_LEN, resl);
        hash_sm3(context, resl, JOIN_LEN, hash_result);
        write(clnt_sock, hash_result, HASH_LEN);
        print(hash_result, HASH_LEN, time, 1);
    }
    catch (...)
    {
        printf("Client socket %d error! This thread has crashed!\n", clnt_sock);
    }
    close(clnt_sock);
    std::string s = "Calc[" + time + "]" + "terminated!\n";
    std::cout << s;
    free(resl);
    free(hash_result);
}

void ocall_print(unsigned char *buf, size_t l)
{
    for (int i = 0; i < l; i++)
        printf("%02x", *(buf + i));
    printf("\n");
}

void print(unsigned char *bytes, int len, std::string time, int op)
{
    std::string buff;
    if (op == 0)
        buff += "UIDi for Calc[";
    else if (op == 1)
        buff += "The Calc[";
    buff += time;
    buff += ']';
    if (op == 0)
        buff += ":";
    else if (op == 1)
        buff += " HASH result is as follows:";
    for (int i = 0; i < len; i++)
    {
        int high = bytes[i] / 16, low = bytes[i] % 16;
        buff += (high < 10) ? ('0' + high) : ('a' + (high - 10));
        buff += (low < 10) ? ('0' + low) : ('a' + (low - 10));
    }
    buff += '\n';
    std::cout << buff;
}